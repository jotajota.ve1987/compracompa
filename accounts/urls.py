from django.urls import path, include
from .views import CustomLoginView, CustomLogoutView, UserDetailView, UpdateUser, PublicProfileView


urlpatterns = [
    path('compa/ingresar', CustomLoginView.as_view(), name='login'),
    path('compa/salir', CustomLogoutView.as_view(), name='logout'),
    path('compa/modificar', UpdateUser.as_view(), name='modificar'),
    path('compa/miperfil/<int:pk>', UserDetailView.as_view(), name='detalles'),
    path('compa/<int:pk>', PublicProfileView.as_view(), name='perfi_publico'),
]
