from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from .models import CustomUser
from core.admin import ComentarioInline
user = get_user_model()

class CustomUserChangeForm(UserChangeForm):
    """
    Se crea un nuevo formulario de cambios de usuario
    """
    
    class Meta(UserChangeForm.Meta):
        model = CustomUser
        fields = '__all__'

class CustomUserAdmin(UserAdmin):
    """
    Se crea una clase del Admin
    """
    form = CustomUserChangeForm
    inlines=[ComentarioInline]
    fieldsets = UserAdmin.fieldsets + (
        ('Compañero', {
            'fields': ('moderador', 'dni', 'fecha_nacimiento', 'genero', 'telefono', 'direccion', 'ciudad', 'provincia'),
        }),
        
    )


admin.site.register(user, CustomUserAdmin)