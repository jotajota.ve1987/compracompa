from django.shortcuts import render
from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView, DetailView
from django.urls import reverse_lazy, path
from django.contrib import messages
from .models import CustomUser



class CustomLoginView(LoginView):
    redirect_authenticated_user = True
    
   # def get_success_url(self):
    #    return reverse_lazy('') 
    
    def form_invalid(self, form):
        messages.error(self.request,'Invalid username or password')
        return self.render_to_response(self.get_context_data(form=form))

class CustomLogoutView(LoginView):
    redirect_authenticated_user = True

class UserDetailView(LoginRequiredMixin, DetailView):
    #Solo para el usuario logueado
    model = CustomUser
    template_name = 'detail_user.html'

class PublicProfileView(DetailView):
    #Vista para todos los usuarios
    model = CustomUser
    template_name = 'detail_public_user.html'

class UpdateUser(LoginRequiredMixin, UpdateView):
    model = CustomUser
    fields = ['genero', 'telefono', 'ciudad', 'provincia']
    template_name = 'update_user.html'
    success_url = reverse_lazy('home')

