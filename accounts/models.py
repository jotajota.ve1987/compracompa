from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.db import models


#choice availables

PROVINCIAS = (
    ('C', 'Ciudad Autónoma de Buenos Aires'),
    ('B', 'Buenos Aires'),
    ('K', 'Catamarca'),
    ('H', 'Chaco'),
    ('U', 'Chubut'),
    ('X', 'Córdoba'),
    ('W', 'Corrientes'),  
    ('E', 'Entre Ríos'),
    ('P', 'Formosa'),
    ('Y', 'Jujuy'),
    ('L', 'La Pampa'),
    ('F', 'La Rioja'),
    ('M', 'Mendoza'),
    ('N', 'Misiones'),
    ('Q', 'Neuquén'),
    ('R', 'Río Negro'),
    ('A', 'Salta'),
    ('J', 'San Juan'),
    ('D', 'San Luis'),
    ('Z', 'Santa Cruz'),
    ('S', 'Santa Fe'),
    ('G', 'Santiago del Estero'),
    ('V', 'Tierra del Fuego'),
    ('T', 'Tucumán'),
)

GENEROS = [
    ("M", "Masculino"),
    ("F", "Femenino"),
    ("O", "Otro"),
    ("N", "Prefiero no decir"),
    ("MTF", "Mujer Transgénero"),
    ("FTM", "Hombre Transgénero"),
    ("NB", "No Binario"),
    ("GF", "Género Fluida"),
    ("AG", "Agénero"),
    ("BD", "Bigénero"),
    ("DE", "Demigénero"),
    ("QT", "Queer"),
    ("IT", "Intersex"),
]

class CustomUser(AbstractUser):

    """
    Usuario Personalizado
    """
    
    # foto_perfil = models.ImageField(_("Foto de Perfil"), upload_to='profile_img/', blank=True, null=True)
    dni = models.PositiveIntegerField(_("DNI"), null=True)
    fecha_nacimiento = models.DateField(_("Fecha de Nacimiento"), auto_now=False, auto_now_add=False, blank=True, null=True)
    genero = models.CharField(_("Género"), max_length=10, choices=GENEROS, null=True)
    telefono = models.PositiveIntegerField(_("Telefono"), blank=True, null=True)
    direccion = models.CharField(_("Dirección"), max_length=255, blank=True, null=True)
    ciudad = models.CharField(_("Ciudad"), max_length=50, blank=True, null=True)
    provincia = models.CharField(_("Provincia"), max_length=50, choices=PROVINCIAS, default="V")
    moderador = models.BooleanField(_("Moderador"), default=False)

    # @property
    # def edad(self):
    #     if self.fecha_nacimiento:
    #         today = date.today()
    #         edad = today.year - self.fecha_nacimiento.year - ((today.month, today.day) < (self.fecha_nacimiento.month, self.fecha_nacimiento.day))
    #         return edad
    #     return None