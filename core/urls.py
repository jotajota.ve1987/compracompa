from django.urls import path
from core.views import HomeView, StoreCreateView, StoreUpdateView, StoreDetailView, StoreDeleteView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('comercio/crear', StoreCreateView.as_view(), name='crear_comercio'),
    path('comercio/modificar/<int:pk>', StoreUpdateView.as_view(), name='modificar_comercio'),
    path('comercio/borrar/<int:pk>', StoreDeleteView.as_view(), name='borrar_comercio'),
    path('comercio/<int:pk>', StoreDetailView.as_view(), name='detalle_comercio'),
]


