from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, path
from django.contrib import messages
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView, ListView
from .models import Comercio

class HomeView(ListView): 
    model = Comercio
    template_name = 'home.html'

    def get_queryset(self):
        return Comercio.objects.filter(activo=True)
    
class StoreCreateView(LoginRequiredMixin, CreateView):
    model = Comercio
    template_name = 'create_comercio.html'
    fields = '__all__'
    success_url = reverse_lazy('home')

class StoreUpdateView(LoginRequiredMixin, UpdateView):
    model = Comercio
    template_name = 'update_comercio.html'
    fields = '__all__'
    success_url = reverse_lazy('home')

class StoreDeleteView(LoginRequiredMixin, DeleteView):
    model = Comercio
    template_name = 'delete_comercio.html'
    success_url = reverse_lazy('home')

class StoreDetailView(DetailView):
    model = Comercio
    template_name = 'detail_comercio.html'






