from django.db import models
from django.utils.translation import gettext_lazy as _

TIPO_BENEFICIO = [
    ('descuentos', 'Descuentos'),
    ('promocion', 'Promoción'),
    ('envio_gratis', 'Envío Gratis'),
    ('compra_x_obsequio', 'Compra X y lleva un obsequio'),
    ('rebajas_temporada', 'Rebajas de Temporada'),
    ('cliente_frecuente', 'Programa de Cliente Frecuente'),
    ('cupones_descuento', 'Cupones de Descuento'),
    ('oferta_flash', 'Ofertas Flash'),
    ('combo_oferta', 'Combos en Oferta'),
    ('tarjeta_fidelidad', 'Tarjeta de Fidelidad'),
    ('regalos_compras_altas', 'Regalos con Compras Altas'),
    ('descuento_cumpleaños', 'Descuento en Cumpleaños'),
    ('programa_referidos', 'Programa de Referidos'),
    ('eventos_especiales', 'Descuentos en Eventos Especiales'),
    ('descuento_redes_sociales', 'Descuento por Seguir en Redes Sociales'),
    ('descuento_primer_compra', 'Descuento en la Primera Compra'),
]

PUNTUACION_COMENTARIO = [
    ('1','1'),
    ('2','2'),
    ('3','3'),
    ('4','4'),
    ('5','5'),
]

class Rubro(models.Model):

    name = models.CharField(_("Nombre"), max_length=50)
    

    class Meta:
        verbose_name = _("rubro")
        verbose_name_plural = _("rubros")

    def __str__(self):
        return self.name

    # def get_absolute_url(self):
    #     return reverse("rubro_detail", kwargs={"pk": self.pk})

class Comercio(models.Model):

    user = models.ForeignKey("accounts.CustomUser", verbose_name=_("Usuario"), on_delete=models.CASCADE, related_name="Comercio")
    activo = models.BooleanField(_("Activo"), default=False)
    name = models.CharField(_("Nombre de Comercio"), max_length=50)
    descripcion = models.TextField(_("Descripción"), max_length=200)
    imagen = models.ImageField(_("Imagen"), upload_to="media/", height_field=None, width_field=None, max_length=None)
    rubros = models.ManyToManyField(Rubro, verbose_name=_("Rubro"), related_name="Rubros")
    facebook = models.URLField(_("Facebook"), max_length=200, blank=True, null=True)
    instagram = models.URLField(_("Facebook"), max_length=200, blank=True, null=True)
    website = models.URLField(_("Website"), max_length=200, blank=True, null=True)
    telefono = models.CharField(_("Teléfono"), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = _("comercio")
        verbose_name_plural = _("comercios")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("comercio_detail", kwargs={"pk": self.pk})

class Beneficio(models.Model):

    comercio = models.ForeignKey(Comercio, verbose_name=_("Comercio"), on_delete=models.CASCADE, related_name="Beneficio")
    tipo_beneficio = models.CharField(_("Tipo de Beneficio"), choices=TIPO_BENEFICIO, max_length=50)
    descripcion = models.CharField(_("Descripción"), max_length=100)

    class Meta:
        verbose_name = _("beneficio")
        verbose_name_plural = _("beneficios")

    def __str__(self):
        return self.descripcion

    # def get_absolute_url(self):
    #     return reverse("beneficio_detail", kwargs={"pk": self.pk})

## Comentarios

class Comentario(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, blank=True)
    texto = models.CharField(_("Comentario"), max_length=200)
    puntuacion = models.CharField(_("Puntuación"), choices=PUNTUACION_COMENTARIO, max_length=100)
    usuario = models.ForeignKey("accounts.CustomUser", verbose_name=_("Usuario"), on_delete=models.CASCADE, related_name="Comentarios")
    comercio = models.ForeignKey(Comercio, verbose_name=_("Comercio"), on_delete=models.CASCADE, related_name="Comentario")

    class Meta:
        verbose_name = _("comentario")
        verbose_name_plural = _("comentarios")

    def __str__(self):
        return self.texto

    # def get_absolute_url(self):
    #     return reverse("comentario_detail", kwargs={"pk": self.pk})
