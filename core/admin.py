from django.contrib import admin
from .models import Rubro, Comercio, Beneficio, Comentario

class BeneficioInline(admin.TabularInline):
    model = Beneficio
    extra = 0

class ComentarioInline(admin.TabularInline):
    model = Comentario
    extra = 0


@admin.register(Comercio)
class ComercioAdmin(admin.ModelAdmin):
    model = Comercio
    inlines = [BeneficioInline, ComentarioInline]

admin.site.register(Rubro)
